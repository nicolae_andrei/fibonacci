package com.xisoft.fibonacci

import android.app.Application
import android.content.Context
import com.xisoft.fibonacci.Page1.MainViewModelFactory
import com.xisoft.fibonacci.Page2.Page2ViewModelFactory
import com.xisoft.fibonacci.network.FiboService
import io.realm.Realm

/////// Services ///////////////////////////////////////////
fun getRetrofitInstance(app: Application): FiboService {
    return (app as FiboApp).retrofitService
}

////// Repositories//////////////////////////////////////////
fun getRepository(app: Application): Repository {
    Repository.initRepositoryVariables(getRetrofitInstance(app))
    return Repository
}

///// View Models Factories //////////////////////////////////
fun getMainActivityViewModelFactory(app: Application): MainViewModelFactory {
    return MainViewModelFactory(getRepository(app))
}

fun getPage2ViewModelFactory(app: Application): Page2ViewModelFactory {
    return Page2ViewModelFactory(getRepository(app))
}