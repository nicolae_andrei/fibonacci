package com.xisoft.fibonacci

import androidx.appcompat.app.AppCompatActivity
import rx.subscriptions.CompositeSubscription

abstract class BaseActivity: AppCompatActivity() {
    lateinit  var subscription: CompositeSubscription

    override fun onResume() {
        super.onResume()
        bindSubscription()
    }

    override fun onPause() {
        super.onPause()
        unbindSubscription()
    }

    private fun bindSubscription() {
        subscription = CompositeSubscription()
        bind()
    }

    private fun unbindSubscription() {
        subscription.unsubscribe()
    }


    abstract fun bind()

}