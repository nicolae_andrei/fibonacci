package com.xisoft.fibonacci

import android.widget.ImageView
import com.squareup.picasso.Picasso

fun Int.isPrime(): Boolean {
    var isPrime = false

    for (i in 2..this / 2) {
        if (this % i == 0) {
            isPrime = true
            break
        }
    }

    return isPrime
}

fun ImageView.loadFromUrl(urlStr: String,
                          sizeWidth: Int = 0,
                          sizeHeight: Int = 0) {
    val requestCreator = Picasso.get().load(urlStr)

    if (sizeWidth > 0 && sizeHeight > 0) {
        requestCreator.resize(sizeWidth, sizeHeight)
            .centerCrop()
    }

    requestCreator
        .into(this)
}
