package com.xisoft.fibonacci

import android.app.Application
import com.xisoft.fibonacci.network.FiboService
import com.xisoft.fibonacci.network.RetrofitClientInstance
import io.realm.Realm

class FiboApp: Application() {
    val retrofitService: FiboService by lazy {
        RetrofitClientInstance().retrofit.create(FiboService::class.java)
    }

    override fun onCreate() {
        super.onCreate()

        initRealm()
    }

    private fun initRealm() {
        Realm.init(this)
    }

    fun getRealmDbInstance(): Realm {
        return Realm.getDefaultInstance()
    }
}