package com.xisoft.fibonacci.network

import com.xisoft.fibonacci.models.LocationResponse
import retrofit2.http.GET
import rx.Observable

interface FiboService {

    @GET("/locations")
    fun getLocationList(): Observable<List<LocationResponse>>
}