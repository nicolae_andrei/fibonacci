package com.xisoft.fibonacci.Page1

import androidx.annotation.NonNull
import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import com.xisoft.fibonacci.Repository

class MainViewModelFactory(val repository: Repository): ViewModelProvider.NewInstanceFactory() {

    override fun <T : ViewModel> create(@NonNull modelClass: Class<T>): T {
        return MainViewModel(repository) as T

    }
}