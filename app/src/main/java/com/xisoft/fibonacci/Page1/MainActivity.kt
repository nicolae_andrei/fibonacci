package com.xisoft.fibonacci.Page1

import android.content.Intent
import android.os.Bundle
import android.widget.Toast
import androidx.lifecycle.ViewModelProviders
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.xisoft.fibonacci.BaseActivity
import com.xisoft.fibonacci.Page2.Page2Activity
import com.xisoft.fibonacci.R
import com.xisoft.fibonacci.getMainActivityViewModelFactory
import com.xisoft.fibonacci.models.FibonacciElement
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : BaseActivity() {
    private lateinit var mainViewModel: MainViewModel

    private val nrOfFibonacciElements = 45
    private var fibonacciList : MutableList<FibonacciElement> = arrayListOf()
    private val fibonacciAdapter =
        FibonacciAdapter(R.layout.item_fibonacci)

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        initViewModel()
        initUI()
        initData()
        initListeners()
    }

    override fun bind() {}

    private fun initViewModel() {
        val mainViewModelFactory: MainViewModelFactory = getMainActivityViewModelFactory(application)
        mainViewModel = ViewModelProviders.of(this, mainViewModelFactory).get(MainViewModel::class.java)
    }

    private fun initUI() {
        val layoutManager: RecyclerView.LayoutManager = LinearLayoutManager(this,
                                                                            LinearLayoutManager.VERTICAL,
                                                                            false)
        recyclerView.hasFixedSize()
        recyclerView.layoutManager = layoutManager
        recyclerView.adapter = fibonacciAdapter
    }

    private fun initData() {
        var firstValue = 0
        var secondValue = 1

        for (i in 1..nrOfFibonacciElements) {
            val sum = firstValue + secondValue
            firstValue = secondValue
            secondValue = sum

            fibonacciList.add(FibonacciElement(firstValue,false))
        }

        fibonacciAdapter.setFibonacciList(fibonacciList)
    }

    private fun initListeners() {
        fibonacciAdapter.fibonacciElemButtonClickListener = { isChecked: Boolean, position: Int ->
            if (isChecked) {
                mainViewModel.setFibonacciNumber(fibonacciList[position].value)
                goToPage2()
            } else {
                Toast.makeText(this@MainActivity,  "Checkbox is Unchecked", Toast.LENGTH_SHORT).show()
            }
        }
        fibonacciAdapter.fibonacciElemCheckboxClickListener = { isChecked, position ->
            fibonacciList[position].isChecked = isChecked
            fibonacciAdapter.notifyItemChanged(position)
        }
    }

    private fun goToPage2() {
        intent = Intent(this@MainActivity, Page2Activity::class.java)
        startActivity(intent)
    }
}
