package com.xisoft.fibonacci.Page1

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.xisoft.fibonacci.isPrime
import com.xisoft.fibonacci.loadFromUrl
import com.xisoft.fibonacci.models.FibonacciElement
import kotlinx.android.synthetic.main.item_fibonacci.view.*

class FibonacciAdapter(private val layoutResId:Int): RecyclerView.Adapter<FibonacciAdapter.ViewHolder>() {
    private var fibonacciList: MutableList<FibonacciElement> = arrayListOf()
    lateinit var fibonacciElemButtonClickListener: ((isChecked: Boolean, position: Int) -> Unit)
    lateinit var fibonacciElemCheckboxClickListener: ((isChecked: Boolean, position: Int) -> Unit)

    fun setFibonacciList(fibonacciList: MutableList<FibonacciElement>) {
        this.fibonacciList = fibonacciList
        notifyDataSetChanged()
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val view = LayoutInflater.from(parent.context).inflate(layoutResId, parent, false)
        return ViewHolder(view)
    }

    override fun getItemCount(): Int {
        return fibonacciList.size
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.bind(fibonacciList[position], position)
    }

    inner class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        fun bind(fibonacciElement: FibonacciElement, position: Int) {
            with(itemView) {
                fiboNumber.text = fibonacciElement.value.toString()
                checkBox.isChecked = fibonacciElement.isChecked

                checkBox.setOnClickListener {
                    fibonacciElemCheckboxClickListener.invoke(checkBox.isChecked, position)
                }
                button.setOnClickListener {
                    fibonacciElemButtonClickListener.invoke(fibonacciElement.isChecked, position)
                }

                if (fibonacciElement.value.isPrime()) {
                    imageView.visibility = View.INVISIBLE
                    checkBox.visibility = View.VISIBLE
                    button.visibility = View.VISIBLE
                } else {
                    imageView.visibility = View.VISIBLE
                    imageView.loadFromUrl(getRandomImageUrl(), 50, 70)
                    checkBox.visibility = View.INVISIBLE
                    button.visibility = View.INVISIBLE
                }
            }
        }

        private fun getRandomImageUrl(): String {
            val randomSeedValue =  java.util.Random().nextInt(100) + 1
            return "https://picsum.photos/id/$randomSeedValue/50/80"
        }

    }
}