package com.xisoft.fibonacci.Page1

import androidx.lifecycle.ViewModel
import com.xisoft.fibonacci.Repository

class MainViewModel(val repository: Repository): ViewModel() {

    fun setFibonacciNumber(number: Int) {
        repository.setFibonacciNumber(number)
    }
}