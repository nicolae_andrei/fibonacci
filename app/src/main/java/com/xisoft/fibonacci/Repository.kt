package com.xisoft.fibonacci

import com.xisoft.fibonacci.models.LocationResponse
import com.xisoft.fibonacci.network.FiboService
import rx.Observable
import rx.Subscriber
import rx.schedulers.Schedulers
import rx.subjects.PublishSubject

object Repository {
    private lateinit var fiboService: FiboService
    private val locationSubject: PublishSubject<LocationResponse> = PublishSubject.create()
    private var currentFibonacciNumber = 0
    private var hasMadeFirstRequest = false


    fun initRepositoryVariables(fiboService: FiboService) {
        this.fiboService = fiboService

    }


    fun setFibonacciNumber(number: Int) {
        currentFibonacciNumber = number
    }

    fun getFibonacciNumber(): Int {
        return currentFibonacciNumber
    }

    fun getLocationData(): Observable<LocationResponse> {
        return locationSubject.onBackpressureBuffer()
    }

    fun makeApiRequest() {
        fiboService.getLocationList()
            .onBackpressureBuffer()
            .subscribeOn(Schedulers.io())
            .subscribe(object: Subscriber<List<LocationResponse>>() {
                 override fun onCompleted() {}
                 override fun onError(e: Throwable) {
                    if (hasMadeFirstRequest) {
                        locationSubject.onNext(null)
                    }
                 }
                 override fun onNext(locations: List<LocationResponse>) {
                     if (locations.isNotEmpty()) {
                         hasMadeFirstRequest = true
                         locationSubject.onNext(locations[0])
                     }
                 }
            })
    }
}