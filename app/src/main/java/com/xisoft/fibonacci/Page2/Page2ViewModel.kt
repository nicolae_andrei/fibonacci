package com.xisoft.fibonacci.Page2

import androidx.lifecycle.ViewModel
import com.xisoft.fibonacci.Repository
import com.xisoft.fibonacci.models.Location
import com.xisoft.fibonacci.models.LocationResponse
import io.realm.RealmResults
import rx.Observable

class Page2ViewModel(val repository: Repository): ViewModel() {

    fun getFibonacciNumber(): Int {
        return repository.getFibonacciNumber()
    }

    fun makeApiRequest() {
        repository.makeApiRequest()
    }

    fun getLocationData(): Observable<LocationResponse> {
        return repository.getLocationData()
    }
}