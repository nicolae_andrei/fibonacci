package com.xisoft.fibonacci.Page2

import android.os.Bundle
import androidx.lifecycle.ViewModelProviders
import com.xisoft.fibonacci.*
import com.xisoft.fibonacci.R
import com.xisoft.fibonacci.models.Location
import com.xisoft.fibonacci.models.LocationResponse
import io.realm.*

import kotlinx.android.synthetic.main.activity_page2.*
import kotlinx.android.synthetic.main.content_page2.*
import rx.android.schedulers.AndroidSchedulers
import rx.schedulers.Schedulers

class Page2Activity : BaseActivity() {
    private lateinit var page2ViewModel: Page2ViewModel
    private lateinit var realmInstance: Realm


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_page2)
        setSupportActionBar(toolbar)

        realmInstance = Realm.getDefaultInstance()
        initViewModel()
        initData()
        page2ViewModel.makeApiRequest()
    }

    override fun bind() {
        subscription.add(page2ViewModel.getLocationData()
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe{ locationResponse ->
                if (locationResponse == null) {
                     getDataFromDb()
                } else {
                    saveLocationToDB(locationResponse)
                    showApiData(locationResponse)
                }


            })
    }

    private fun getDataFromDb() {
       val results: Location? = getLocationFromDBListenable()
            showApiData(LocationResponse(
                results?.title, results?.name,
                results?.pictureUrlList?.toMutableList()))
    }

    private fun showApiData(locationResponse: LocationResponse?) {
        locationTitle.text = locationResponse?.title
        locationName.text = locationResponse?.name
        locationResponse?.pictureUrlList?.let {
            locationImage.loadFromUrl(locationResponse.pictureUrlList[0])
        }
    }

    private fun initViewModel() {
        val page2ViewModelFactory: Page2ViewModelFactory = getPage2ViewModelFactory(application)
        page2ViewModel = ViewModelProviders.of(this, page2ViewModelFactory).get(Page2ViewModel::class.java)
    }


    private fun initData() {
        this.title = page2ViewModel.getFibonacciNumber().toString()
    }

    private fun saveLocationToDB(locationResponse: LocationResponse) {
        realmInstance.executeTransaction {
            val realmList: RealmList<String> = RealmList()
            locationResponse.pictureUrlList?.let { it1 -> realmList.addAll(it1) }
            val location = Location(1L,
                locationResponse.title,
                locationResponse.name,
                realmList)
            it.copyToRealmOrUpdate(location)
        }
    }

    private fun getLocationFromDBListenable() : Location? {
        return realmInstance.where(Location::class.java)
            .equalTo("id", 1L)
            .findFirst()
    }

}
