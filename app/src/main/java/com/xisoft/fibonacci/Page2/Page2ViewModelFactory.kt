package com.xisoft.fibonacci.Page2

import androidx.annotation.NonNull
import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import com.xisoft.fibonacci.Repository

class Page2ViewModelFactory(val repository: Repository): ViewModelProvider.NewInstanceFactory() {

    override fun <T : ViewModel> create(@NonNull modelClass: Class<T>): T {
        return Page2ViewModel(repository) as T

    }
}