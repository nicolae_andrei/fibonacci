package com.xisoft.fibonacci.models

import io.realm.Realm
import io.realm.RealmList
import io.realm.RealmObject
import io.realm.annotations.PrimaryKey

open class Location(
    @PrimaryKey open var id: Long = 0,
    open var title: String? = "",
    open var name: String? = "",
    open var pictureUrlList: RealmList<String>? = RealmList()
): RealmObject()