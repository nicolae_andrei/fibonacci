package com.xisoft.fibonacci.models

import com.google.gson.annotations.SerializedName

class LocationResponse (
    @SerializedName("title")
    val title: String?,
    @SerializedName("name")
    val name: String?,
    @SerializedName("pictures")
    val pictureUrlList: MutableList<String>?
)